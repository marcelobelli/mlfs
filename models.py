from django.db import models

# Create your models here.
class Cliente(models.Model):
    nome = models.CharField(name='Nome', max_length=100)
    documento = models.CharField(name='CPF ou CNPJ', max_length=14)
    endereco = models.CharField(name='Endereço', max_length=255)
    telefone = models.CharField(name='Telefone', max_length=20)
    email = models.EmailField(name='E-mail')


class Servidor(models.Model):
    tag = models.CharField(name='Tag', max_length=20)
    ip = models.IPAddressField(name='IP')
    hostname = models.CharField(name='Hostname', max_length=50)
    obs = models.TextField(name='Observações', blank=True)
